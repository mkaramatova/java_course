package chapter7;

import java.util.Scanner;

public class DayOfTheWeek {

    public static void main(String[] args) {
        String [] DaysOfTheWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of a day of the week:");
        int n = scanner.nextInt();

        scanner.close();
        if (n >= 1 && n <= 7) {
            System.out.println(DaysOfTheWeek[n-1]);
        }
        else {
            System.out.println("Number is not valid");
        }

    }
}
