package chapter8;

import java.util.Scanner;

public class PasswordValidator {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter username:");
        String username = scanner.next();
        System.out.println("Enter old password:");
        String oldPass = scanner.next();
        System.out.println("Enter new password:");
        String newPass = scanner.next();
        scanner.close();

        if (isValid(username, oldPass, newPass)) {
            System.out.println("Password changed successfully!");
        }
        else {
            System.out.println("The new password is not valid, must follow those rules:");
            System.out.println("is at least 8 characters long");
            System.out.println("contains an uppercase letter");
            System.out.println("contains a special character");
            System.out.println("does not contain the username");
            System.out.println("is not the same as the old password");
        }

    }

    static boolean isValid (String username, String oldPassword, String newPassword) {

        boolean passwordIsValid;

        if (newPassword.length() < 8
                || newPassword.equals(newPassword.toLowerCase())
                || newPassword.matches("[a-zA-Z0-9 ]*")
                || newPassword.contains(username)
                || newPassword.equals(oldPassword)) {
            passwordIsValid = false;
        }
        else {
            passwordIsValid = true;
        }

        return passwordIsValid;
    }
}
