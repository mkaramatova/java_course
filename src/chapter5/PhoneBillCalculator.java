package chapter5;

import java.text.DecimalFormat;
import java.util.Scanner;

public class PhoneBillCalculator {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the plan fee:");
        double planFee = scanner.nextDouble();
        System.out.println("Enter number of overage minutes:");
        int overageMinutes = scanner.nextInt();

        scanner.close();

        double overageFees = CalculateOverageFees(overageMinutes);
        double tax = CalculateTax(planFee + overageFees);

        CalculateTotalBill(planFee, tax, overageFees);
    }

    static double CalculateTax (double subtotal) {
        double taxOnSubtotal = 0.15;
        double finalTax = subtotal * taxOnSubtotal;

        return finalTax;
    }

    static double CalculateOverageFees (int overageMinutes) {
        double overageTax = 0.25;
        double overageFees = overageMinutes * overageTax;

        return overageFees;
    }

    static void CalculateTotalBill (double planFee, double finalTax, double overageFees) {
        double totalBill = planFee + finalTax + overageFees;

        DecimalFormat df2 = new DecimalFormat("#.##");

        System.out.println("Plan fee: " + df2.format(planFee));
        System.out.println("Overage: " + df2.format(overageFees));
        System.out.println("Tax: " + df2.format(finalTax));
        System.out.println("Total: " + df2.format(totalBill));
    }
}


