package chapter9;

public class TasteTester {
    public static void main(String[] args) {
        Cake cake = new Cake("cherry");
        cake.setPrice(19.80);
        System.out.println("Flavor: " + cake.getFlavor());
        System.out.println("Price: " + cake.getPrice());

        BirthdayCake birthdayCake = new BirthdayCake();
        birthdayCake.setPrice(19.90);
        System.out.println("BirthdayCake flavor: " + birthdayCake.getFlavor());
        System.out.println("BirthdayCake price: " + birthdayCake.getPrice());

        WeddingCake weddingCake = new WeddingCake();
        weddingCake.setFlavor("peach");
        System.out.println("WeddingCake flavor: " + weddingCake.getFlavor());
        System.out.println("WeddingCake price: " + weddingCake.getPrice());

    }
}
