package chapter6;

import java.text.DecimalFormat;

public class PhoneBill {

    public int id;
    public double baseCost;
    public int allottedMinutes;
    public int minutesUsed;

    public PhoneBill(){
        id = 1;
        baseCost = 9.99;
        allottedMinutes = 200;
        minutesUsed = 220;
    }

    public PhoneBill(int id){
        this.id = id;
        baseCost = 9.99;
        allottedMinutes = 200;
        minutesUsed = 220;
    }

    public PhoneBill (int id, double baseCost, int allottedMinutes, int minutesUsed) {
        this.id = id;
        this.baseCost = baseCost;
        this.allottedMinutes = allottedMinutes;
        this.minutesUsed = minutesUsed;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public double getBaseCost(){
        return baseCost;
    }

    public void setBaseCost(double baseCost){
        this.baseCost = baseCost;
    }

    public int getAllottedMinutes(){
        return allottedMinutes;
    }

    public void setAllottedMinutes(int allottedMinutes){
        this.id = allottedMinutes;
    }

    public int getMinutesUsed(){
        return minutesUsed;
    }

    public void setMinutesUsed(int minutesUsed){
        this.minutesUsed = minutesUsed;
    }

    public double CalculateTax () {
        double taxOnSubtotal = 0.15;
        return (baseCost + CalculateOverageFees ()) * taxOnSubtotal;
    }

    public double CalculateOverageFees () {
        if(minutesUsed > allottedMinutes) {
            double overageTax = 0.25;
            return overageTax * (minutesUsed - allottedMinutes);
        }
        return 0;
    }

    public void CalculateTotalBill () {
        double totalBill = baseCost + CalculateTax() + CalculateOverageFees();

        DecimalFormat df2 = new DecimalFormat("#.##");

        System.out.println("Bill id: " + df2.format(id));
        System.out.println("Plan fee: " + df2.format(baseCost));
        System.out.println("Overage: " + df2.format(CalculateOverageFees()));
        System.out.println("Tax: " + df2.format(CalculateTax()));
        System.out.println("Total: " + df2.format(totalBill));
    }

}
